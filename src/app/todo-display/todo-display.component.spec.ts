import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoDisplayComponent } from './todo-display.component';

describe('TodoDisplayComponent', () => {
  let component: TodoDisplayComponent;
  let fixture: ComponentFixture<TodoDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('onClick should emit onItemClick event', () => {
    // Arrange
    let itemIndex = -1;
    component.onItemClick.subscribe({
      next: (i) => itemIndex = i
    });
    expect(itemIndex).toEqual(-1);

    // Act
    component.onClick(2);

    // Assert
    expect(itemIndex).toEqual(2);
  });

});
