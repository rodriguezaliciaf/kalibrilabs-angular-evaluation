import { TestBed, inject } from '@angular/core/testing';

import { RepositoryService } from './repository.service';

describe('RepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RepositoryService]
    });
  });

  it('should be created', inject([RepositoryService], (service: RepositoryService) => {
    expect(service).toBeTruthy();
  }));

  // TODO: this test should be broken down into multiple unit tests that check for each of the scenarios given.
  it('saveTodos() properly adds elements to the repository', inject([RepositoryService], (service: RepositoryService) => {
    // Doesn't add the null value
    service.saveTodo(null);
    let results: string[] = service.getTodos();
    expect(results).toEqual([]);

    // Doesn't add the empty string
    service.saveTodo("");
    results = service.getTodos();
    expect(results).toEqual([]);
    
    // Correctly add one element
    service.saveTodo("foo");
    results = service.getTodos();
    expect(results).toEqual(["foo"]);

    // Correctly add a second elements
    service.saveTodo("bar");
    results = service.getTodos();
    expect(results).toEqual(["foo", "bar"]);
  }));

  // TODO: this test should be broken down into multiple unit tests that check for each of the scenarios given.
  it('deleteTodo() properly deletes elements from the repository', inject([RepositoryService], (service: RepositoryService) => {
    // Doesn't delete a null value
    service.deleteTodo(null);
    let results: string[] = service.getTodos();
    expect(results).toEqual([]);

    // Doesn't delete not-found value
    service.deleteTodo(-1);
    results = service.getTodos();
    expect(results).toEqual([]);
    
    // Correctly deletes the first element
    // // Arrange
    service.saveTodo("foo");
    results = service.getTodos();
    expect(results).toEqual(["foo"]);
    // // Act
    service.deleteTodo(0);
    // // Assert
    results = service.getTodos();
    expect(results).toEqual([]);

    // Correctly deletes the second element
    // // Arrange
    service.saveTodo("foo");
    service.saveTodo("bar");
    results = service.getTodos();
    expect(results).toEqual(["foo", "bar"]);
    // // Act
    service.deleteTodo(1);
    // // Assert
    results = service.getTodos();
    expect(results).toEqual(["foo"]);

    // Correctly deletes the last element
    // // Arrange
    service.saveTodo("bar");
    service.saveTodo("boom");
    results = service.getTodos();
    expect(results).toEqual(["foo", "bar", "boom"]);
    // // Act
    service.deleteTodo(results.length - 1);
    // // Assert
    results = service.getTodos();
    expect(results).toEqual(["foo", "bar"]);
  }));

  // TODO: this test should be broken down into multiple unit tests that check for each of the scenarios given.
  it('getTodos() returns all the right elements', inject([RepositoryService], (service: RepositoryService) => {
    // Verify we return nothing when nothing exists
    let results: string[] = service.getTodos();
    expect(results).toEqual([]);
    
    // Correctly return one element
    service.saveTodo("foo");
    results = service.getTodos();
    expect(results).toEqual(["foo"]);

    // Correctly return multiple elements
    service.saveTodo("bar");
    results = service.getTodos();
    expect(results).toEqual(["foo", "bar"]);
  }));

});
