import { Injectable } from '@angular/core';

@Injectable()
export class RepositoryService {

  private todoItems: string[] = [];

  constructor() { }

  saveTodo(item: string) {
    if (!item) { return; }
    this.todoItems = [...this.todoItems, item];
  }

  deleteTodo(index: number) {
    if (index < 0) { return ;}
    this.todoItems.splice(index, 1);
  }

  getTodos(): string[] {
    return this.todoItems;
  }

}
